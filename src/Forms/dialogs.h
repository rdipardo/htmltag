/*
  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this file,
  You can obtain one at https://mozilla.org/MPL/2.0/.

  Copyright (c) 2024 Robert Di Pardo and Contributors
*/
#ifndef HTMLTAG_DIALOGS_H
#define HTMLTAG_DIALOGS_H

#include "resource.h"

/* arabic */
#define AR_ABOUT_DLG 0x0400
#define AR_UNICODE_DLG 0x0800
#define AR_ABOUT_CAPTION "المكون الإضافي HTML Tag لبرنامج ++Notepad"
#define AR_UNICODE_DLG_CAPTION "تغيير بادئة أحرف Unicode"
/* catalan */
#define CA_ABOUT_DLG 0x0401
#define CA_UNICODE_DLG 0x0801
#define CA_ABOUT_CAPTION "Complement HTML Tag per Notepad++"
#define CA_UNICODE_DLG_CAPTION "Canviar el prefix de caràcters Unicode"
/* chineseSimplified */
#define ZH_ABOUT_DLG 0x0402
#define ZH_UNICODE_DLG 0x0802
#define ZH_ABOUT_CAPTION "Notepad++ 的 HTML Tag 插件"
#define ZH_UNICODE_DLG_CAPTION "更改 Unicode 字符前缀"
/* dutch */
#define NL_ABOUT_DLG 0x0403
#define NL_UNICODE_DLG 0x0803
#define NL_ABOUT_CAPTION "HTML Tag-plugin voor Notepad++"
#define NL_UNICODE_DLG_CAPTION "Wijzig het Unicode-teken voorvoegsel"
/* farsi */
#define FA_ABOUT_DLG 0x0404
#define FA_UNICODE_DLG 0x0804
#define FA_ABOUT_CAPTION "پلاگین HTML Tag برای ++Notepad"
#define FA_UNICODE_DLG_CAPTION "پیشوند کاراکترهای یونیکد را تغییر دهید"
/* french */
#define FR_ABOUT_DLG 0x0405
#define FR_UNICODE_DLG 0x0805
#define FR_ABOUT_CAPTION "Module d'extension HTML Tag pour Notepad++"
#define FR_UNICODE_DLG_CAPTION "Modifier le préfixe de caractère Unicode"
/* german */
#define DE_ABOUT_DLG 0x0406
#define DE_UNICODE_DLG 0x0806
#define DE_ABOUT_CAPTION "HTML Tag-Plugin für Notepad++"
#define DE_UNICODE_DLG_CAPTION "Unicode-Zeichenpräfix ändern"
/* hebrew */
#define HE_ABOUT_DLG 0x0407
#define HE_UNICODE_DLG 0x0807
#define HE_ABOUT_CAPTION "תוסף HTML Tag עבור ++Notepad"
#define HE_UNICODE_DLG_CAPTION "שנה את הקידומת של תווי Unicode"
/* hindi */
#define HI_ABOUT_DLG 0x0408
#define HI_UNICODE_DLG 0x0808
#define HI_ABOUT_CAPTION "Notepad++ के लिए HTML Tag प्लगइन"
#define HI_UNICODE_DLG_CAPTION "यूनिकोड वर्ण उपसर्ग बदलें"
/* italian */
#define IT_ABOUT_DLG 0x0409
#define IT_UNICODE_DLG 0x0809
#define IT_ABOUT_CAPTION "Plugin HTML Tag per Notepad++"
#define IT_UNICODE_DLG_CAPTION "Modifica il prefisso del carattere Unicode"
/* japanese */
#define JP_ABOUT_DLG 0x040A
#define JP_UNICODE_DLG 0x080A
#define JP_ABOUT_CAPTION "Notepad++ 用 HTML Tag プラグイン"
#define JP_UNICODE_DLG_CAPTION "Unicode 文字プレフィックスを変更"
/* korean */
#define KO_ABOUT_DLG 0x040B
#define KO_UNICODE_DLG 0x080B
#define KO_ABOUT_CAPTION "Notepad++용 HTML Tag 플러그인"
#define KO_UNICODE_DLG_CAPTION "유니코드 문자 접두사 변경"
/* polish */
#define PL_ABOUT_DLG 0x040C
#define PL_UNICODE_DLG 0x080C
#define PL_ABOUT_CAPTION "Wtyczka HTML Tag do Notepad++"
#define PL_UNICODE_DLG_CAPTION "Zmień przedrostek znaku Unicode"
/* portuguese */
#define PT_ABOUT_DLG 0x040D
#define PT_UNICODE_DLG 0x080D
#define PT_ABOUT_CAPTION "Plugin HTML Tag para Notepad++"
#define PT_UNICODE_DLG_CAPTION "Modificar prefixo de caracteres Unicode"
/* brazilian_portuguese */
#define BR_PT_ABOUT_DLG 0x040E
#define BR_PT_UNICODE_DLG 0x080E
#define BR_PT_ABOUT_CAPTION PT_ABOUT_CAPTION
#define BR_PT_UNICODE_DLG_CAPTION "Alterar prefixo de caracteres Unicode"
/* romanian */
#define RO_ABOUT_DLG 0x040F
#define RO_UNICODE_DLG 0x080F
#define RO_ABOUT_CAPTION "Plugin HTML Tag pentru Notepad++"
#define RO_UNICODE_DLG_CAPTION "Schimbă prefixul caracterelor Unicode"
/* russian */
#define RU_ABOUT_DLG 0x0410
#define RU_UNICODE_DLG 0x0810
#define RU_ABOUT_CAPTION "Плагин HTML Tag для Notepad++"
#define RU_UNICODE_DLG_CAPTION "Изменить префикс символов Unicode"
/* sinhala */
#define SI_ABOUT_DLG 0x0420
#define SI_UNICODE_DLG 0x0820
#define SI_ABOUT_CAPTION "Notepad++ සඳහා HTML Tag ප්ලගිනය"
#define SI_UNICODE_DLG_CAPTION "යුනිකෝඩ් අක්ෂර උපසර්ගය වෙනස් කරන්න"
/* spanish, spanish_ar */
#define ES_ABOUT_DLG 0x0430
#define ES_UNICODE_DLG 0x0830
#define ES_ABOUT_CAPTION "Complemento HTML Tag para Notepad++"
#define ES_UNICODE_DLG_CAPTION "Cambiar el prefijo del carácter Unicode"
/* tamil */
#define TA_ABOUT_DLG 0x0450
#define TA_UNICODE_DLG 0x0850
#define TA_ABOUT_CAPTION "Notepad++ க்கான HTML Tag செருகுநிரல்"
#define TA_UNICODE_DLG_CAPTION "யூனிகோட் எழுத்து முன்னொட்டை மாற்று"
/* ukrainian */
#define UK_ABOUT_DLG 0x0460
#define UK_UNICODE_DLG 0x0860
#define UK_ABOUT_CAPTION "Плагін HTML Tag для Notepad++"
#define UK_UNICODE_DLG_CAPTION "Змінити префікс символів Unicode"

#endif /* ~HTMLTAG_DIALOGS_H */
